<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SiteController extends AbstractController
{
    #[Route('/', name: 'site')]
    public function index(): Response
    {
        return $this->render('site/index.html.twig', [
            'controller_name' => 'SiteController',
        ]);
    }

    // public function register(UserPasswordHasherInterface $passwordHasher): Response
    // {
    //     // ... e.g. get the user data from a registration form
    //     $user = new User(...);
    //     $plaintextPassword = ...;

    //     // hash the password (based on the security.yaml config for the $user class)
    //     $hashedPassword = $passwordHasher->hashPassword(
    //         $user,
    //         $plaintextPassword
    //     );
    //     $user->setPassword($hashedPassword);

    //     // ...
    // }
}
