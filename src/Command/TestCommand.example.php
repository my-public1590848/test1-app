<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:example-command')]
class ExampleCommandCommand extends Command
{
    // описание команды, отображаемое при запуске "php bin/console list"
    protected static $defaultDescription = 'Example Command';

    // ...
    protected function configure(): void
    {
        $this
            // сообщение помощи команды, отображаемое при запуске команды с опцией "--help"
            ->setHelp('This command just example...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // ... введите здесь код, чтобы создать пользователя

        // этот метод должен вернуть целое число с "кодом завершения"
        // команды. Вы также можете использовать это константы, чтобы сделать код более читаемым

        // вернуть это, если при выполнении команды не было проблем
        // (эквивалентно возвращению int(0))
        return Command::SUCCESS;

        // или вернуть это, если во время выполнения возникла ошибка
        // (эквивалентно возвращению int(1))
        // return Command::FAILURE;

        // или вернуть это, чтобы указать на неправильное использование команды, например, невалидные опции
        // или отсутствующие аргументы (равноценно возвращению int(2))
        // return Command::INVALID
    }
}