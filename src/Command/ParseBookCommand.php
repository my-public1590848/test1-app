<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Doctrine\ORM\EntityManagerInterface;

#[AsCommand(name: 'app:parse-book')]
class ParseBookCommand extends Command
{
    private EntityManagerInterface $entityManager;
    private HttpClientInterface $httpClient;
    private ParameterBagInterface $params;

    protected static $defaultDescription = 'Creates a new user.';

    public function __construct(EntityManagerInterface $entityManagerInterface, HttpClientInterface $httpClientInterface, ParameterBagInterface $params)
    {
        $this->entityManager = $entityManagerInterface;
        $this->httpClient = $httpClientInterface;
        $this->params = $params; // Можно изящнее, особенно если настройки сайта касающиеся бизнес-логики клиента лежат в БД
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setHelp('This command allows you to create a user...');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        echo "Starting parsing...\n";

        $newBook = [];
        $updateBook = [];

        $url = $this->params->get('app.source-url');

        if (str_contains($url, 'gitlab') || str_contains($url, 'github')) $url = str_replace('/blob/', '/raw/', $url);

        echo $url . "\n";

        $response = $this->httpClient->request(
            'GET',
            $url
        );

        $books = $this->getDoctrine()->getRepository('Books')->findAll();

        foreach ($response->toArray() as $item) {

        }

        print_r($response->toArray());

        // Конец

        echo "Success!!!" . (count($newBook) > 0 ? " " . count($newBook) . " book added!" : "")  . (count($updateBook) > 0 ? " " . count($updateBook) . " book updated!" : "") . "\n";

        return Command::SUCCESS;
    }
}
