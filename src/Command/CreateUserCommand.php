<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Mime\Address;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use App\Security\EmailVerifier;
use App\Entity\User;

#[AsCommand(name: 'app:create-user')]
class CreateUserCommand extends Command
{
    private EntityManagerInterface $entityManager;
    private UserPasswordHasherInterface $passwordHasher;
    private EmailVerifier $emailVerifier;

    protected static $defaultDescription = 'Creates a new user.';

    public function __construct(EntityManagerInterface $entityManagerInterface, UserPasswordHasherInterface $passwordHasherInterface, EmailVerifier $emailVerifier)
    {
        $this->emailVerifier = $emailVerifier;
        $this->entityManager = $entityManagerInterface;
        $this->passwordHasher = $passwordHasherInterface;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setHelp('This command allows you to create a user...');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        echo "Creating new user...\nEnter new user email: ";
        $email = trim(fgets(STDIN));

        echo "Enter new user password: ";
        $pass = trim(fgets(STDIN));

        // (!!!) Место для валидатора

        $user = new User();

        $user->setEmail($email);
        $user->setPassword(
            $this->passwordHasher->hashPassword(
                $user,
                $pass
            )
        );

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->emailVerifier->sendEmailConfirmation(
            'app_verify_email',
            $user,
            (new TemplatedEmail())
                ->from(new Address('dyplass@gmail.com', 'Nikita Symfony Тестовое'))
                ->to($user->getEmail())
                ->subject('Please Confirm your Email')
                ->htmlTemplate('registration/confirmation_email.html.twig')
        );

        echo "New user" . $email . "successfully created!!!\n";

        return Command::SUCCESS;
    }
}
