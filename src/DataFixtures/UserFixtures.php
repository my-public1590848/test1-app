<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $admin = new User();
        $admin->setEmail('admin1@test.com');
        $admin->setPassword('$2y$13$NLKUG3nmzBOoByxcJFTj.uuVTrGs2Py0sEKhAorftYE1gvkeavjYe'); //admin1pass
        $admin->setRoles(['ROLE_ADMIN']);

        $manager->persist($admin);

        $user = new User();
        $user->setEmail('user1@test.com');
        $user->setPassword('$2y$13$cQtwpp3cTM4VN8/f7DqOYuVMyDEZPLGN4zJBxyeQTbYz/ReIfpsQq'); //user1pass
        $user->setRoles([]);

        $manager->persist($user);

        $manager->flush();

    }
}
